#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import shutil
import colorama
import xml.etree.ElementTree as ET
from colorama import Fore

# Réinitialisation de la couleur après chaque 'print'
colorama.init(autoreset=True)

# Test pour vérifier si la saisie d'un nom de fichier est correcte
if len(sys.argv) < 2:
    print(Fore.RED + '----------------------------------------------------')
    print(Fore.RED + 'Veuillez préciser le nom du fichier JXL à traiter...')
    print(Fore.RED + '----------------------------------------------------')
    exit(0)

# Récupération du nom du fichier et de son extension
document = os.path.abspath(sys.argv[1])
(pathname, fullname) = os.path.split(document)
(filename, extension) = os.path.splitext(fullname)

# Test pour vérifier si le fichier et son extension sont corrects
if os.path.exists(document) != True or extension != '.jxl':
    print(Fore.RED + '------------------------------------------')
    print(Fore.RED + 'Veuillez indiquer un fichier JXL valide...')
    print(Fore.RED + '------------------------------------------')
    exit(0)

# Affichage du processus en cours
print('Traitement du fichier ' + Fore.YELLOW + document + Fore.RESET + ' en cours...')

# Lecture du fichier JXL et initialisation de l'élément 'tree'
tree = ET.parse(document)
root = tree.getroot()

# Initialisation des variables
list_ID = []
list_Date = []
list_Code = []
list_Desc1 = []
list_Desc2 = []
list_CSV = []

# Recherche des informations sur le système de projection
for element in root.findall('./Environment/CoordinateSystem'):
    print('├─> ' + 'Système : ' + Fore.RED + element.find('./SystemName').text)
    print('├─> ' + 'Zone : ' + Fore.RED + element.find('./ZoneName').text)
    print('├─> ' + 'Datum : ' + Fore.RED + element.find('./DatumName').text)

# Recherche des ID et des dates dans la section 'PointRecord'
for element in root.findall('./FieldBook/PointRecord'):
    list_ID.append(element.get('ID'))
    list_Date.append(element.get('TimeStamp').split('T')[0])

# Création d'un dictionnaire pour stocker les ID et les dates
index = dict(zip(list_ID, list_Date))

# Suppression des doublons dans la liste des dates
list_clean = sorted(list(set(list_Date)))

# Traitement du champ 'Code'
for data in root.findall('./Reductions/Point'):
    if data.find('./Code').text is not None:
        list_Code.append(data.find('./Code').text)
    else:
        list_Code.append('')

# Traitement du champ 'Description1'
for data in root.findall('./Reductions/Point'):
    if data.find('./Description1') is not None and data.find('./Description1').text is not None:
        list_Desc1.append(data.find('./Description1').text)
    else:
        list_Desc1.append('')
if len(''.join(list_Desc1)) > 0:
    print('├─> ' + 'Champ supplémentaire n°1 : ' + Fore.CYAN + data.find('./Description1').get('Name'))

# Traitement du champ 'Description2'
for data in root.findall('./Reductions/Point'):
    if data.find('./Description2') is not None and data.find('./Description2').text is not None:
        list_Desc2.append(data.find('./Description2').text)
    else:
        list_Desc2.append('')
if len(''.join(list_Desc2)) > 0:
    print('├─> ' + 'Champ supplémentaire n°2 : ' + Fore.CYAN + data.find('./Description2').get('Name'))

# Affichage du nombre de dates contenues dans le fichier JXL
print('├─> ' + 'Nombre de dates identifiées : ' + Fore.YELLOW + str(len(list_clean)))

# Création de plusieurs fichiers CSV, selon les dates
for item in list_clean:
    with open(pathname + '/' + filename + '_' + item + '.csv', 'w') as output:
        for data in root.findall('./Reductions/Point'):
            if index[data.find('./ID').text] == item:
                Mat = data.find('./Name').text
                Coord_E = format(float(data.find('./Grid/East').text), '.3f')
                Coord_N = format(float(data.find('./Grid/North').text), '.3f')
                Coord_Z = format(float(data.find('./Grid/Elevation').text), '.3f')

                # Traitement particulier pour le champ 'Code'
                if data.find('./Code').text is not None:
                    if data.find('./Code').text.find(',') == -1 and sum(',' in sub0 for sub0 in list_Code) == 0:
                        Code = data.find('./Code').text
                    elif data.find('./Code').text.find(',') == -1 and sum(',' in sub0 for sub0 in list_Code) != len(list_Code):
                        Code = data.find('./Code').text + ','
                    else:
                        Code = data.find('./Code').text
                elif data.find('./Code').text is None and sum(',' in sub0 for sub0 in list_Code) != 0:
                    Code = ','
                else:
                    Code = ''

                # Traitement particulier pour le champ 'Description1'
                if data.find('./Description1') is not None and data.find('./Description1').text is not None:
                    if data.find('./Description1').text.find(',') == -1 and sum(',' in sub1 for sub1 in list_Desc1) == 0:
                        Desc1 = ',' + data.find('./Description1').text
                    elif data.find('./Description1').text.find(',') == -1 and sum(',' in sub1 for sub1 in list_Desc1) != len(list_Desc1):
                        Desc1 = ',' + data.find('./Description1').text + ','
                    else:
                        Desc1 = ',' + data.find('./Description1').text
                else:
                    if len(''.join(list_Desc1)) > 0 and sum(',' in sub1 for sub1 in list_Desc1) == 0:
                        Desc1 = ','
                    elif len(''.join(list_Desc1)) > 0 and sum(',' in sub1 for sub1 in list_Desc1) > 0:
                        Desc1 = ',' + ','
                    else:
                        Desc1 = ''

                # Traitement particulier pour le champ 'Description2'
                if data.find('./Description2') is not None and data.find('./Description2').text is not None:
                    if data.find('./Description2').text.find(',') == -1 and sum(',' in sub2 for sub2 in list_Desc2) == 0:
                        Desc2 = ',' + data.find('./Description2').text
                    elif data.find('./Description2').text.find(',') == -1 and sum(',' in sub2 for sub2 in list_Desc2) != len(list_Desc2):
                        Desc2 = ',' + data.find('./Description2').text + ','
                    else:
                        Desc2 = ',' + data.find('./Description2').text
                else:
                    if len(''.join(list_Desc2)) > 0 and sum(',' in sub2 for sub2 in list_Desc2) == 0:
                        Desc2 = ','
                    elif len(''.join(list_Desc2)) > 0 and sum(',' in sub2 for sub2 in list_Desc2) > 0:
                        Desc2 = ',' + ','
                    else:
                        Desc2 = ''

                # Construction de la ligne à écrire dans le fichier CSV
                output.write(Mat + ',' + Coord_E + ',' + Coord_N + ',' + Coord_Z + ',' + Code + Desc1 + Desc2 + '\n')

    # Affichage du résultat (fichier CSV selon la date)
    with open(pathname + '/' + filename + '_' + item + '.csv', 'r') as csv_daily:
        print('├─> ' + 'Création du fichier ' + Fore.GREEN + filename + '_' + item + '.csv' + Fore.RESET + ' : ' + str(len(csv_daily.readlines())) + ' pts')

    # Ajout du nom de fichier à la liste globale
    list_CSV.append(pathname + '/' + filename + '_' + item + '.csv')

# Création d'un fichier CSV avec tous les points
with open(pathname + '/' + filename + '.csv', 'w') as outfile:
    for record in list_CSV:
        with open(record, 'r') as infile:
            shutil.copyfileobj(infile, outfile)

# Affichage du résultat (fichier CSV final)
with open(pathname + '/' + filename + '.csv', 'r') as csv_final:
    print('└─> ' + 'Création du fichier ' + Fore.YELLOW + filename + '.csv' + Fore.RESET + ' : ' + str(len(csv_final.readlines())) + ' pts')
