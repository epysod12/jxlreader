# jxlreader.py

Ce script permet d'extraire les données d'un fichier JXL (Trimble JobXML) et de les enregistrer au format CSV.

## Prérequis

  * python3-colorama

## Utilisation

Le lancement s'effectue dans un terminal :

```
$ python3 jxlreader.py /chemin/vers/mon/fichier.jxl
```

## Fonctionnement

Un fichier JXL est composé de trois parties :

* `FieldBook`
* `Reductions`
* `Environment`

Le script `jxlreader.py` récupère les ID et les dates de tous les points dans la section `Fieldbook`, puis croise les informations avec les données de la section `Reductions` pour récupérer les coordonnées. Les points sont ensuite enregistrés dans des fichiers CSV triés par date, ainsi que dans un fichier CSV final.

Les informations sur le système de projection, récupérées dans la section `Environment`, sont affichées pendant le processus.

## Licence

Le programme ci-joint est publié sous licence GPLv3.  
[www.gnu.org/licenses/gpl-3.0.fr.html](https://www.gnu.org/licenses/gpl-3.0.fr.html)
